# Python Google Spreadsheets Helper

## Goal

A simple retrieval of data using Gogole Spreadsheets API is a non-trivial process, and the goal of this project is to
make it a simple, single-line process:

```GSpreadsheetsHelper("user@mygoogleappsdomain.com").get_spreadsheet("1g3RsTKRe3xijfxCJynphBWX0qosy")```

## Usage

### Setting Up Credentials

This module is built to be used with Google API service account. [Read more here](https://developers.google.com/identity/protocols/OAuth2ServiceAccount)

A valid credentials file must be provided to use most of the classes in this modules. By default the credentials will
be read from `~/.google-api-credentials.json`

Example of the credentials file:
```json
    {
      "private_key_id": "<private_key_id>",
      "private_key": "-----BEGIN PRIVATE KEY-----\n<private_key>\n-----END PRIVATE KEY-----\n",
      "client_email": "<client_email>",
      "client_id": "<client_id>",
      "type": "service_account"
    }
```

When using service account with Google Apps, `user_email` specifies which user the API should act under,
when accessing the data. `user_email` parameter is available in `GSpreadsheetsHelper` and `GSpreadsheetsClient` classes.

### Retrieving Data

Spreadsheets can be retrieved in multiple ways:

#### CSV Data Using GSpreadsheetClient

```
client = GSpreadsheetsClient("user@mygoogleappsdomain.com")
client.authorize()
csv_data = client.retrieve_by_key(spreadsheet_key="1g3RsTKRe3xijfxCJynphBWX0qosy", sheet_number=2)
```

`csv_data` now contains Google Spreadsheet in CSV format.

**Sheet numbers are 0-based (Sheet 1 is sheet_number=0)**

#### CSV Data Using GSpreadsheetsHelper

```
gs_helper = GSpreadsheetsHelper("user@mygoogleappsdomain.com")
csv_data = gs_helper.get_spreadsheet_csv_data(spreadsheet_key="1g3RsTKRe3xijfxCJynphBWX0qosy", sheet_number=2)
```

`csv_data` now contains Google Spreadsheet in CSV format.

#### GSpreadsheetsData Using GSpreadsheetsHelper
 
```
gs_helper = GSpreadsheetsHelper("user@mygoogleappsdomain.com")
gs_data = gs_helper.get_spreadsheet(spreadsheet_key="1g3RsTKRe3xijfxCJynphBWX0qosy", sheet_number=2)
```

`gs_data` now contains an instance of `GSpreadsheetsData` which allows you to access rows, columns using the following methods:

- `get_row`
- `get_row_by_key`
- `get_dict`
- `row_to_sorted_row`
- `get_sorted_rows_dict`

### Working with GSpreadsheetsData

TODO: Needs to be documented.
"""

This module is built to be used with Google API service account. Read more at:
https://developers.google.com/identity/protocols/OAuth2ServiceAccount

A valid credentials file must be provided to use most of the classes in this modules. By default the credentials will
be read from ~/.google-api-credentials.json

Example of the credentials file::
    {
      "private_key_id": "<private_key_id>",
      "private_key": "-----BEGIN PRIVATE KEY-----\n<private_key>\n-----END PRIVATE KEY-----\n",
      "client_email": "<client_email>",
      "client_id": "<client_id>",
      "type": "service_account"
    }

"""

import unittest
import os.path
import re
from copy import copy, deepcopy
from StringIO import StringIO
from csv import DictReader
import pandas as pd
from oauth2client.client import SignedJwtAssertionCredentials
import json
import httplib2


_DEFAULT_CREDENTIALS_FILE = "~/.google-api-credentials.json"
_DATA_FORMAT_CSV = 'csv'
_MAX_SHEETS = 1024


class GSpreadsheet(object):

    def __init__(self, key):
        """
        Initializes a new spreadsheet object with a given spreadsheet key. Can be used to retrieve data from
        Google Spreadsheets using Google API.
        :param key: Spreadsheet key.
        """
        super(GSpreadsheet, self).__init__()
        self.key = key
        self.sheet_data = [None for i in range(0, _MAX_SHEETS)]
        self.client = None
        self.loaded_sheets = set()

    def load_sheet_data(self, sheet_number=0, index_field=None, skip_rows=0):
        """
        Loads a single sheet into `sheets_data` array.
        :param sheet_number: 0-based sheet number.
        :param index_field: Index field to be used by `GSpreadSheetsData` instance.
        :return: Data retrieved for the given sheet number.
        """
        assert self.client is not None, "`client` property must be set before the spreadsheet data can be retrieved."
        assert type(self.client) is GSpreadsheetsClient
        assert self.client.is_authorized(), "`client` must be authorized before spreadsheet data can be retrieved."

        csv_data = self.client.retrieve_by_key(self.key, sheet_number, data_format=_DATA_FORMAT_CSV)
        self.sheet_data[sheet_number] = GSpreadsheetsData(csv_data, index_field=index_field, skip_rows=skip_rows)
        self.loaded_sheets.add(sheet_number)
        return self.sheet_data[sheet_number]


class GSpreadsheetsClient(object):

    _URL_FORMAT = "https://spreadsheets.google.com/feeds/download/spreadsheets/Export?key=%s&exportFormat=%s&gid=%i"

    def __init__(self, user_email=None, credentials_file=None):
        """
        Initializes a Google Spreadsheets client.
        :param str user_email: When using service account with Google Apps, `user_email`
                               specifies which user the API should act under, when accessing the data.
        :param credentials_file: Credentials file that will be used to authenticate the service account.
        :return:
        """
        super(GSpreadsheetsClient, self).__init__()

        self.user_email = user_email

        if credentials_file is None:
            credentials_file = _DEFAULT_CREDENTIALS_FILE

        self.credentials_file = credentials_file
        self._http_auth = None

    def authorize(self):
        """
        Sets up client authorization for future Google API requests. Must be called before retrieving spreadsheets.
        :return: None.
        """

        with open(os.path.expanduser(self.credentials_file), "r") as fp:
            credentials_dict = json.load(fp)

        credentials = SignedJwtAssertionCredentials(credentials_dict['client_email'], credentials_dict['private_key'],
            'https://spreadsheets.google.com/feeds/', sub=self.user_email)

        self._http_auth = credentials.authorize(httplib2.Http())

    def is_authorized(self):
        return self._http_auth is not None

    def retrieve_by_key(self, spreadsheet_key, sheet_number=0, data_format="csv"):
        """
        Retrieves a spreadsheet sheet in the specified format (defaults to first sheet in CSV format).
        :param GSpreadsheet spreadsheet: GSpreadsheet object (contains a spreadsheet key to be retrieved).
        :param int sheet_number: 0-based spreadsheet sheet number.
        :param data_format: spreadsheet format.
        :return: spreadsheet format contents (a single sheet in the format specified by `format` parameter).
        """
        assert self.is_authorized(), "Instance method `authorize` must be called before attempting to retrieve a spreadsheet."
        response, content = self._http_auth.request(self._URL_FORMAT % (spreadsheet_key, data_format, sheet_number))
        return content

#==============================================================================


class GSpreadsheetsData(object):

    def __init__(self, csv_data, index_field=None, skip_rows=0):
        assert type(csv_data) is str
        assert type(index_field) is str or index_field is None

        self._index_field = index_field
        self._skip_rows = skip_rows

        fp = StringIO(csv_data)

        csv_reader = DictReader(fp)
        self._field_names = deepcopy(csv_reader.fieldnames)
        csv_rows = []

        skipped_rows = 0

        for csv_row in csv_reader:
            if skip_rows > 0 and skipped_rows < skip_rows:
                skipped_rows += 1
                continue

            csv_rows.append(self._sanitize_csv_row(csv_row))
        fp.close()

        self._df = pd.DataFrame(csv_rows)

    @property
    def index_field(self):
        return self._index_field

    @property
    def skip_rows(self):
        return self._skip_rows

    @staticmethod
    def _sanitize_csv_row(row):

        result = {}

        for key, value in row.iteritems():
            result[key] = str(value).strip()

        return result

    def get_row(self, index):
        """
        Retrieves a single row by row index.
        :param index: index of the row.
        :return: dict with the row's values.
        """

        assert type(index) is int

        result = self._df.ix[index].to_dict()
        if result is not None and type(result) is dict:
            return result

        return None

    def get_row_by_key(self, key):
        """
        Retrieves the row by the key (uses index field).
        :param key: key as indexed by index field.
        :return: dict with the row's values.
        """

        assert type(key) is str
        assert self._index_field is not None

        normalized_key = re.escape(key.strip())
        pattern = '^%s$' % (normalized_key,)

        result = self._df[self._df[self._index_field].str.contains(pattern, case=False)]

        if result is not None:
            result = result.to_dict('records')

        if len(result) == 1 and type(result[0]) is dict:
            return result[0]
        elif len(result) > 1:
            raise Exception('Duplicate rows found for key "%s" (field "%s")' % (key, self._index_field))

        return None

    def get_dict(self, limit_count=0):
        """
        Retrieves the data grouped by the unique keys, as indexed by index field.
        :return: dict, where keys are the unique table's keys, and values are rows (as dicts).
        """

        assert self._index_field is not None

        unique_keys = self._df[self._index_field].unique()

        result_dict = {}
        count = 0

        for key in unique_keys:
            if limit_count > 0 and count >= limit_count:
                break
            if len(key) == 0:
                continue
            result_dict[key] = self.get_row_by_key(key)
            count += 1

        return result_dict

    def row_to_sorted_row(self, row):

        result_list = []

        for field_name in self._field_names:
            result_list.append([field_name, row[field_name]])

        return result_list

    def get_sorted_rows_dict(self, limit_count=0):

        result_dict = {}

        for key, value in self.get_dict(limit_count).iteritems():
            result_dict[key] = self.row_to_sorted_row(value)

        return result_dict

#==============================================================================


class GSpreadsheetsHelper(object):

    def __init__(self, user_email=None, credentials_file=None):
        """
        Initializes a helper class to retrieve Google Spreadsheets.
        :param str user_email: When using service account with Google Apps, `user_email`
                               specifies which user the API should act under, when accessing the data.
        :param credentials_file: Credentials file that will be used to authenticate the service account.
        :return: None.
        """
        if user_email is not None:
            assert type(user_email) is str
        if credentials_file is not None:
            assert type(credentials_file) is str
        self._client = None
        self._user_email = user_email
        self._credentials_file = credentials_file


    def _initialize_client(self):
        """
        Initializes the `GSpreadsheetsClient` instance used to access Google Spreadsheets API. Performs client
        authorization.
        :return: None.
        """
        if self._client is None:
            self._client = GSpreadsheetsClient(self._user_email, self._credentials_file)
            self._client.authorize()

    def get_spreadsheet_csv_data(self, spreadsheet_key, sheet_number=0):
        """
        Retrieves a single sheet from a spreadsheet by its key and sheet number in CSV format.
        :param spreadsheet_key: Google Spreadsheet key to retrieve (typically part of the spreadsheet URL).
        :param sheet_number: 0-based sheet number.
        :return: Sheet contents in CSV format.
        """
        self._initialize_client()

        csv_data = self._client.retrieve_by_key(spreadsheet_key, sheet_number, _DATA_FORMAT_CSV)

        return csv_data

    def get_spreadsheet(self, spreadsheet_key, sheet_number=0, index_field=None, skip_rows=0):
        """
        Retrieves a single sheet from a spreadsheet by its key and sheet number.
        :param spreadsheet_key: Google Spreadsheet key to retrieve (typically part of the spreadsheet URL).
        :param sheet_number: 0-based sheet number.
        :return: GSpreadsheet object with data pre-loaded for the given sheet nubmer.
        """
        self._initialize_client()
        spreadsheet = GSpreadsheet(spreadsheet_key)
        spreadsheet.load_sheet_data(sheet_number=sheet_number, index_field=index_field, skip_rows=skip_rows)
        return spreadsheet


#==============================================================================
# Unit Tests
#==============================================================================

class GSpreadsheetsTests(unittest.TestCase):

    def testSpreadsheetsDuplicateData(self):

        csv_data = "Name,Field 1,Field 2,Field 3,Field 4\n" \
                   "name 1,value 1,value 2,value 3,1\n" \
                   "name 2,value 4,value 5,value 6,2\n" \
                   "name 3 ,value 7,value 8,value 9,3\n" \
                   "name 4,value 10,value 11,value 12,4\n" \
                   "name 4,value 13,value 14, value 15,5\n"

        gs_data = GSpreadsheetsData(csv_data, 'Name')
        self.assertRaises(Exception, gs_data.get_row_by_key, 'name 4')

    def testSpreadsheetsData(self):

        csv_data = ""

        self.assertIsNotNone(GSpreadsheetsData(csv_data, 'Name'))

        csv_data = "Name,Field 1,Field 2,Field 3,Field 4\n" \
                   "name 1,value 1,value 2,value 3,1\n" \
                   "name 2,value 4,value 5,value 6,2\n" \
                   "name 3 ,value 7,value 8,value 9,3\n" \
                   "name 4,value 10,value 11,value 12,4\n"

        gs_data = GSpreadsheetsData(csv_data, 'Name')

        self.assertIsNone(gs_data.get_row_by_key('non-existing name'))

        result_dict = gs_data.get_row_by_key('name 2')

        self.assertIs(dict, type(result_dict))
        self.assertEqual('name 2', result_dict['Name'])

        result_dict = gs_data.get_dict()
        self.assertTrue('value 4', result_dict['name 2']['Field 1'])
        self.assertTrue('4', result_dict['name 4']['Field 4'])

        self.assertEqual('value 6', gs_data.get_row(1)['Field 3'])

        sorted_row = gs_data.row_to_sorted_row(gs_data.get_row(2))
        self.assertEqual('Name', sorted_row[0][0])

        sorted_rows_dict = gs_data.get_sorted_rows_dict()
        self.assertEqual('Name', sorted_rows_dict['name 4'][0][0])


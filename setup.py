__author__ = 'Alex Prykhodko (Pixellu LLC)'

from distutils.core import setup

setup(
    name='gspreadsheets',
    version="0.2.4",
    description="Google Spreadsheets Helpers (CSV data retrieval, CSV data parsing).",
    author="Alex Prykhodko",
    author_email="alex@pixellu.com",
    py_modules=['gspreadsheets'],
    install_requires=['pandas', 'oauth2client<2.0.0']
)
